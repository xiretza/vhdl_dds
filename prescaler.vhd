-- divides an input clock by a variable factor of 2^n

library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity prescaler is
	generic (
		MAX_DIVIDE_POWER : natural
	);
	port (
		clk_in : in std_logic;

		divide_power : in natural range 0 to MAX_DIVIDE_POWER;
		clk_out      : out std_logic
	);
end prescaler;

architecture rtl of prescaler is
	signal chain : std_logic_vector(MAX_DIVIDE_POWER downto 0) := (others => '0');
begin
	chain(0) <= clk_in;

	gen: for i in 1 to MAX_DIVIDE_POWER generate
		process(chain(i-1))
		begin
			if rising_edge(chain(i-1)) then
				chain(i) <= not chain(i);
			end if;
		end process;
	end generate;

	clk_out <= chain(divide_power);
end rtl;
