library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity delta_sigma is
	generic (
		RESOLUTION : positive
	);
	port (
		clk : in std_logic;

		value     : in signed(RESOLUTION-1 downto 0);
		modulated : out std_logic
	);
end delta_sigma;

architecture rtl of delta_sigma is
	signal difference : signed(RESOLUTION downto 0);
	signal integrated : signed(RESOLUTION downto 0) := (others => '0');
	signal comp_out   : signed(RESOLUTION-1 downto 0);
begin
	subtractor: process(value, comp_out)
	begin
		difference <= resize(value, RESOLUTION+1) - resize(comp_out, RESOLUTION+1);
	end process;

	integrator: process(clk)
	begin
		if rising_edge(clk) then
			integrated <= integrated + difference;
		end if;
	end process;

	comparator: process(integrated)
	begin
		if integrated < 0 then
			modulated <= '0';
			comp_out  <= ('1', others => '0');
		else
			modulated <= '1';
			comp_out  <= ('0', others => '1');
		end if;
	end process;
end rtl;
