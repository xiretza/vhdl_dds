library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity dds_tb is
end dds_tb;

architecture sim of dds_tb is
	signal clk, reset, stop : std_logic;
	signal step, prescale_power : natural;
	signal output_raw : signed(11 downto 0);
	signal output_pwm : std_logic;
	signal output_delta_sigma : std_logic;
begin
	uut: entity work.dds
		generic map (
			RESOLUTION    => 12,
			COUNTER_WIDTH => 9
		)
		port map (
			clk        => clk,
			prescale_power => prescale_power,
			step       => step,
			output_raw => output_raw,
			output_pwm => output_pwm,
			output_delta_sigma => output_delta_sigma
		);

	clock: entity work.clock_gen
		generic map (
			T_pulse => 5 ns,
			T_period => 10 ns
		)
		port map (
			clk => clk,
			reset => reset,
			stop => stop
		);

	stimulus: process
	begin
		stop <= '0';
		step <= 1;
		prescale_power <= 1;
		wait for 20 us;

		step <= 5;
		wait for 2 us;

		step <= 1;
		wait for 3 us;

		prescale_power <= 3;
		wait for 50 us;

		stop <= '1';
		wait;
	end process;
end sim;
