library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all,
    ieee.math_real.all;

entity lut is
	generic (
		ADDRESS_WIDTH : natural;
		DATA_WIDTH    : natural
	);
	port (
		clk         : in std_logic;
		wave_select : in std_logic_vector(1 downto 0);

		address     : in std_logic_vector(ADDRESS_WIDTH-1 downto 0);
		data        : out signed(DATA_WIDTH-1 downto 0) := (others => '0')
	);
end lut;

architecture rtl of lut is
	constant SAMPLES     : natural := 2 ** ADDRESS_WIDTH;
	constant MAX_VALUE   : natural := 2 ** (DATA_WIDTH - 1) - 1;

	type table_t is array(0 to SAMPLES-1)
		of signed(DATA_WIDTH-1 downto 0);

	function generate_sine_lut return table_t is
		variable tb : table_t;
	begin
		for i in tb'range loop
			tb(i) := to_signed(
					integer(
						sin(2.0 * MATH_PI * real(i) / real(SAMPLES))
						* real(MAX_VALUE)
					),
					DATA_WIDTH
				);
		end loop;

		return tb;
	end function;

	-- resize a vector src from a given length src_size to dst_size, either
	-- cutting off LSB or padding LSB with zeroes
	function adjust_size(
		src : std_logic_vector;
		src_size, dst_size : natural
	) return std_logic_vector is
	begin
		if src_size > dst_size then
			return src(src'high downto src'high - dst_size - 1);
		else
			return src & (dst_size - src_size - 1 downto 0 => '0');
		end if;
	end function;

	constant sin_table : table_t := generate_sine_lut;
begin
	lookup: process(clk)
	begin
		if rising_edge(clk) then
			case wave_select is
				when "00" =>
					-- sine
					data <= sin_table(to_integer(unsigned(address)));
				when "01" =>
					-- sawtooth
					data <= signed(adjust_size(address, ADDRESS_WIDTH, DATA_WIDTH));
				when "10" =>
					-- square
					if to_integer(unsigned(address)) >= SAMPLES / 2 then
						data <= ('0', others => '1');
					else
						data <= ('1', others => '0');
					end if;
				when others =>
					data <= (others => '0');
			end case;
		end if;
	end process;
end rtl;
