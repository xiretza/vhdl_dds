library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity pwm is
	generic (
		RESOLUTION : positive := 8
	);
	port (
		clk : in std_logic;

		value     : in signed(RESOLUTION-1 downto 0);
		modulated : out std_logic
	);
end pwm;

architecture rtl of pwm is
	constant COUNT_MAX : natural := 2 ** (RESOLUTION - 1) - 1;
	signal counter : signed(RESOLUTION-1 downto 0) := (others => '0');

	signal latched_value : signed(RESOLUTION-1 downto 0);
begin
	count: process(clk)
	begin
		if rising_edge(clk) then
			if counter = COUNT_MAX then
				latched_value <= value;
			end if;

			counter <= counter + 1;
		end if;
	end process;

	modulated <= '1' when counter <= latched_value else '0';
end rtl;
