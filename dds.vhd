library ieee;
use ieee.std_logic_1164.all,
    ieee.numeric_std.all;

entity dds is
	generic (
		-- number of bits of output resolution
		RESOLUTION : natural := 12;
		-- width of counter/LUT address
		COUNTER_WIDTH : natural := 9;
		MAX_PRESCALE_POWER : natural := 8
	);
	port (
		clk : in std_logic;

		prescale_power : in natural range 0 to MAX_PRESCALE_POWER;
		step           : in natural range 0 to 2 ** COUNTER_WIDTH - 1;

		output_raw : out signed(RESOLUTION-1 downto 0);
		output_pwm : out std_logic;
		output_delta_sigma : out std_logic
	);
end dds;

architecture rtl of dds is
	constant COUNTER_MAX : natural := 2 ** COUNTER_WIDTH - 1;
	signal counter : natural range 0 to COUNTER_MAX := 0;

	signal prescaled_clk : std_logic;

	signal lut_addr : std_logic_vector(COUNTER_WIDTH-1 downto 0);
	signal lut_data : signed(RESOLUTION-1 downto 0);
begin
	lut_addr <= std_logic_vector(to_unsigned(counter, lut_addr'length));

	lut: entity work.lut
		generic map (
			ADDRESS_WIDTH => COUNTER_WIDTH,
			DATA_WIDTH    => RESOLUTION
		)
		port map (
			clk         => prescaled_clk,
			wave_select => "00",

			address     => lut_addr,
			data        => lut_data
		);

	delta_sigma: entity work.delta_sigma
		generic map (
			RESOLUTION => RESOLUTION
		)
		port map (
			clk   => clk,
			value => lut_data,

			modulated => output_delta_sigma
		);

	pwm: entity work.pwm
		generic map (
			RESOLUTION => 6
		)
		port map (
			clk   => clk,
			value => lut_data(RESOLUTION-1 downto RESOLUTION-6),

			modulated => output_pwm
		);

	prescale: entity work.prescaler
		generic map (
			MAX_DIVIDE_POWER => MAX_PRESCALE_POWER
		)
		port map (
			clk_in => clk,
			divide_power => prescale_power,

			clk_out => prescaled_clk
		);

	count: process(prescaled_clk)
	begin
		if rising_edge(prescaled_clk) then
			if counter + step > COUNTER_MAX then
				counter <= step - (COUNTER_MAX - counter) - 1;
			else
				counter <= counter + step;
			end if;
		end if;
	end process;

	output_raw <= lut_data;
end rtl;
