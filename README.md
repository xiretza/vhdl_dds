# Direct Digital Synthesis (DDS) module in VHDL

This project requires VHDL 2008 compliant tooling.

## Components

### `dds.vhd`

Top-level entity

### `prescaler.vhd`

Clock prescaler, divides an input clock by a variable factor of `2^n`, with the
maximum value of `n` specified by a generic.

### `lut.vhd`

Lookup Table (ROM), also contains a function to populate the ROM with values for
a sine wave.

### `pwm.vhd`

Pulse Width Modulation component, contains a counter and a comparator: when the
count value is less than the input value, the output is high, otherwise low.

### `dds_tb.vhd`, `clock_gen.vhd`

Test bench/simulation sources

## Demonstration

Tested using [ghdl](https://github.com/ghdl/ghdl) and [gtkwave](http://gtkwave.sourceforge.net/):

```sh
ghdl -i --std=08 *.vhd
ghdl -m --std=08 dds_tb
ghdl -r --std=08 dds_tb --wave=dds_tb.ghw

gtkwave dds_tb.ghw dds_tb.gtkw
```


![waveform](waveform.png)
